using FirstWPF.DataAccess.Models;

namespace FirstWPF.DataAccess.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<FirstWPF.DataAccess.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(FirstWPF.DataAccess.Context context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            if (!context.Movies.Any())
            {

                Random rnd = new Random();

                context.Movies.AddOrUpdate(
                    new Movie() {Author = "Roman Pola�ski", Title = "Titanic", Year = 1992},
                    new Movie() {Author = "John Lennon", Title = "Hug me now", Year = 2002},
                    new Movie() {Author = "Lucas Leiva", Title = "Three days in Monaco", Year = 2005},
                    new Movie() {Author = "Marc Twain", Title = "Journey to Boston", Year = 2000},
                    new Movie() {Author = "Alex Jordan", Title = "Cashing Checks for Bears", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Whitney Morgan", Title = "Untying Gump", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Ernestie Mccarthy", Title = "Waiting for Jesus H. Christ", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Candice Gonzales", Title = "Playing with Samus Samuellson", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Rochelle	Wallace", Title = "Playing with Bronzeman", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Anthony Dunn", Title = "Split", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Fred	Fletcher", Title = "Prometheus", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Derrice Briggs", Title = "The Terminator", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Kelly Mendez", Title = "Star Wars: The Force Awakens", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Carolyn Barker", Title = "Jaws", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Frankie Norris", Title = "Arrival", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Nathan Pope", Title = "The Jungle Book", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Martin Henderson", Title = "Hacksaw Ridge", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Vincent Cruz", Title = "Scarface", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Toby	Garner", Title = "Taxi Driver", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Mitchell	Davidson", Title = "Moonlight", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Tyler Casey", Title = "La La Land", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Johnathan Walters", Title = "Inglourious Basterds", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Eric Ryan", Title = "The Shining", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Anthony Gordon", Title = "Chappie", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Willard Olson", Title = "Fight Club", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Bradley Barnes", Title = "Insurgent", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Guillermo Price", Title = "A Clockwork Orange", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Darrell Walker", Title = "Soldier of Payback", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Marvin Anderson", Title = "Maximum Justice", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Alejandro Osborne", Title = "Terminal Overkill", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Marvin Anderson", Title = "Inferno of Assassinations", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Taylor Rowe", Title = "Triple Extermination", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Alfonso Tran", Title = "Master of Extremism", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Harold Burke", Title = "Extreme Surrender", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Eduardo Ball", Title = "War for Retreat", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Roderick	Vasquez", Title = "Instant Prejudice", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Wade	Lyons", Title = "Infinite Trouble", Year = rnd.Next(1900, 2017)},
                    new Movie() {Author = "Theodore Pratt", Title = "Double Punishment", Year = rnd.Next(1900, 2017)});              
            }


            if (!context.Users.Any())
            {
                context.Users.Add(new User() {Login = "michal", PasswordHash = "budzan"});
            }

        }
    }
}
