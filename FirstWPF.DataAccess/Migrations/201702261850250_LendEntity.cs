namespace FirstWPF.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LendEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Lends",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MovieId = c.Int(nullable: false),
                        LendExpireDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Lends");
        }
    }
}
