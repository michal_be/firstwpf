namespace FirstWPF.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpDb : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Lends", "MovieId");
            AddForeignKey("dbo.Lends", "MovieId", "dbo.Movies", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Lends", "MovieId", "dbo.Movies");
            DropIndex("dbo.Lends", new[] { "MovieId" });
        }
    }
}
