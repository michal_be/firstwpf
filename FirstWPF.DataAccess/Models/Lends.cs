﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstWPF.DataAccess.Models
{
    public class Lends
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Movie")]
        public int MovieId { get; set; }

        public DateTime LendExpireDate { get; set; }

        public virtual Movie Movie { get; set; }      
         
        

    }
}
