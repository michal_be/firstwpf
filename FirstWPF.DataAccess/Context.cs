﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirstWPF.DataAccess.Models;

namespace FirstWPF.DataAccess
{
    public class Context: DbContext
    {

        public DbSet<Movie> Movies { get; set; }
        public DbSet<User> Users { get; set; } 
        public DbSet<Lends> Lends { get; set; } 

    }
}
