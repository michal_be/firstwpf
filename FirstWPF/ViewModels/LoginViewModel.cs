﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirstWPF.DataAccess.Models;

namespace FirstWPF.ViewModels
{
    public class LoginViewModel
    {
        public User Login { get; set; }

        public LoginViewModel()
        {
             Login = new User();   
        }
       
    }
}
