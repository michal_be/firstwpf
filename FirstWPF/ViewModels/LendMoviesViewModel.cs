﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirstWPF.DataAccess;
using FirstWPF.DataAccess.Models;

namespace FirstWPF.ViewModels
{
    public class LendMoviesViewModel
    {
        Context context;
        public ObservableCollection<Lends> LendMovies { get; set; } 
        public LendMoviesViewModel()
        {
            context = new Context();
            LoadLendMovies();
        }

        private void LoadLendMovies()
        {
            var lendMovies = context.Lends.ToList();
            ObservableCollection<Lends> lendMoviesCollection = new ObservableCollection<Lends>(lendMovies);

            LendMovies = lendMoviesCollection;
        }
    }
}
