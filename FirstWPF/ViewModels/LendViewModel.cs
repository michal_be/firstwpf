﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirstWPF.DataAccess.Models;

namespace FirstWPF.ViewModels
{
    public class LendViewModel
    {
        public Lends Lend { get; set; }

        public LendViewModel()
        {
            Lend = new Lends();       
        }

    }
}
