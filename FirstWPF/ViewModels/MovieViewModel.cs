﻿using FirstWPF.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using FirstWPF.Annotations;
using FirstWPF.DataAccess;
using FirstWPF.Views;


namespace FirstWPF.ViewModels
{
    public class MovieViewModel  : INotifyPropertyChanged
    {
        Context context;
        private ObservableCollection<Movie> _movieList;
        private ICommand _clickCommand;
        private bool _canExecute = true;
        private string _searchTxt;
        private IList<Movie> _allMovies; 


        public MovieViewModel()
        {
            context = new Context();
            LoadMovies();        
        }
        
      
        public ICommand ClickCommand
        {
            get
            {
                return _clickCommand ?? (_clickCommand = new CommandHandler(() => MyAction(), _canExecute));
            }
        }

        public string SearchTxt
        {
            get
            {
                return _searchTxt;
            }

            set
            {
                _searchTxt = value;
                OnPropertyChanged(nameof(SearchTxt));
                MovieList = new ObservableCollection<Movie>(_allMovies.Where(x => x.Title.Contains(SearchTxt)));
            }
        }

        private void MyAction()
        {
            // wyszukiwanie w datagridzie, pobieranie danych z bazy itp, logika
           
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            
        }

        public ObservableCollection<Movie> MovieList
        {
            get
            {
                return _movieList;
            }

            set
            {
                _movieList = value;
                OnPropertyChanged(nameof(MovieList));
            }
        }

        public void LoadMovies()
        {
            var moviesFromDb = context.Movies.ToList();
            MovieList = new ObservableCollection<Movie>(moviesFromDb);
            _allMovies = new List<Movie>(MovieList);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
