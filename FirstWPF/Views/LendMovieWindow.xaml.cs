﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FirstWPF.DataAccess.Models;
using FirstWPF.ViewModels;
using Context = FirstWPF.DataAccess.Context;

namespace FirstWPF.Views
{
    /// <summary>
    /// Interaction logic for LendMovieWindow.xaml
    /// </summary>
    public partial class LendMovieWindow : Window
    {
        Context context;
        LendViewModel viewModel = new LendViewModel();

        public LendMovieWindow()
        {
            InitializeComponent();
            DataContext = viewModel;
            context = new Context();
        }

        private void BackBtn_OnClick_(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            Close();
        }

        private void LendMovieBtn_Click(object sender, RoutedEventArgs e)
        {
            LendMovie(viewModel);
        }


        private void LendMovie(LendViewModel lendViewModel)
        {
            bool validRequest = CheckIfRequestIsValid(lendViewModel.Lend.MovieId);

            if (validRequest)
            {
                context.Lends.Add(new Lends()
                {
                    LendExpireDate = viewModel.Lend.LendExpireDate,
                    MovieId = viewModel.Lend.MovieId
                });
                context.SaveChanges();
            }
        }

        private bool CheckIfRequestIsValid(int movieId)
        {
          bool movieExists = CheckIfMovieExists(movieId);
          bool movieIsAvailable = CheckIfMovieIsAvailable(movieId);

            if (!movieExists)
            {
                InvalidMovieId invalidMovieId = new InvalidMovieId();
                invalidMovieId.ShowDialog();
                return false;
            }
            else if (!movieIsAvailable)
            {
                MovieIsNotAvailable movieIsNotAvailable = new MovieIsNotAvailable(movieId);
                movieIsNotAvailable.ShowDialog();
                return false;
            }
            else
            {
                return true;
            }
          
        }

        private bool CheckIfMovieIsAvailable(int movieId)
        {
            if (context.Lends.Any(x => x.MovieId == movieId))
            {
                return false;
            }
         
                return true;            
        }

        private bool CheckIfMovieExists(int movieId)
        {
            bool movieExists = context.Movies.Any(x => x.Id == movieId);

            return movieExists;
        }

    }
}
