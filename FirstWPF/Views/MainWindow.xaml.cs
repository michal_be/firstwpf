﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FirstWPF.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MoviesCollectionBtn_Click(object sender, RoutedEventArgs e)
        {
            MoviesCollection moviesCollection = new MoviesCollection();
            moviesCollection.Show();
            Close();
        }

        private void LendMovieBtn_Click(object sender, RoutedEventArgs e)
        {
            LendMovieWindow lendMovieWindow = new LendMovieWindow();
            lendMovieWindow.Show();
            Close();
        }

        private void ReturnMovieBtn_OnClick(object sender, RoutedEventArgs e)
        {
            ReturnMovieWindow returnMovieWindow = new ReturnMovieWindow();
            returnMovieWindow.Show();
            Close();
        }

        private void LendMovies_OnClick(object sender, RoutedEventArgs e)
        {
            LendMoviesCollectionWindow lendMoviesCollectionWindow = new LendMoviesCollectionWindow();
            lendMoviesCollectionWindow.Show();
            Close();
        }
    }
}
