﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FirstWPF.DataAccess;
using FirstWPF.DataAccess.Models;
using FirstWPF.ViewModels;

namespace FirstWPF.Views
{
    /// <summary>
    /// Interaction logic for MovieIsNotAvailable.xaml
    /// </summary>
    public partial class MovieIsNotAvailable : Window
    {
        Context context;
        LendViewModel lendViewModel = new LendViewModel();

        public MovieIsNotAvailable(int movieId)
        {
            InitializeComponent();
            context = new Context();
            LoadLendViewModel(movieId, lendViewModel);
            DataContext = lendViewModel;
        }

        private void LoadLendViewModel(int movieId, LendViewModel lendViewModel)
        {           
            lendViewModel.Lend = context.Lends.SingleOrDefault(x => x.MovieId == movieId);

        }

       

    }
}
