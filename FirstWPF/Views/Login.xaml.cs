﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstWPF.DataAccess;
using FirstWPF.DataAccess.Models;
using FirstWPF.ViewModels;
using FirstWPF.Views;

namespace FirstWPF.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        Context context;
        LoginViewModel viewModel = new LoginViewModel();

        public LoginWindow()
        {
            context = new Context();
            InitializeComponent();
            DataContext = viewModel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //var loginVal = viewModel.Login.Login;
            //var passwordVal = PasswordVal.Password;

            //List<User> users = context.Users.ToList();
            //bool loginIsValid = LoginIsValid(loginVal, passwordVal, users);

            bool loginIsValid = true;

            if (loginIsValid)
            {
                var mainWindow = new MainWindow();
                mainWindow.Show();
                Close();
            }
            else
            {
                var invalidLoginWindow = new InvalidLogin();
                invalidLoginWindow.ShowDialog();
            }          
        }

        private bool LoginIsValid(string login, string password, List<User> users)
        {
            if (users.Any(x => x.Login == login && x.PasswordHash == password))
                return true;

            return false;
        }
    }
}
