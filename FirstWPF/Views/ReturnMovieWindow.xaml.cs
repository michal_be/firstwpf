﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FirstWPF.DataAccess;

namespace FirstWPF.Views
{
    /// <summary>
    /// Interaction logic for ReturnMovieWindow.xaml
    /// </summary>
    public partial class ReturnMovieWindow : Window
    {
        Context context;

        public ReturnMovieWindow()
        {
            InitializeComponent();
            context = new Context();
        }


        private void ReturnMovieBtn_OnClick(object sender, RoutedEventArgs e)
        {
            int movieId = Int32.Parse(IdVal.Text);
            ReturnMovie(movieId);
        }

        private void BackBtn_OnClick(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            Close();
        }

        private void ReturnMovie(int movieId)
        {
            if (!context.Lends.Any(x => x.MovieId == movieId))
            {
                InvalidMovieReturn invalidMovieReturn = new InvalidMovieReturn();
                invalidMovieReturn.ShowDialog();
            }
            else
            {
                var movieToReturn = context.Lends.Single(x => x.MovieId == movieId);
                context.Lends.Remove(movieToReturn);
                context.SaveChanges();
            }           
        }

    }
}
