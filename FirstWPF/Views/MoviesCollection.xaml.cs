﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FirstWPF.ViewModels;

namespace FirstWPF.Views
{
    /// <summary>
    /// Interaction logic for MoviesCollection.xaml
    /// </summary>
    public partial class MoviesCollection : Window
    {
        MovieViewModel viewModel = new MovieViewModel();

        public MoviesCollection()
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        //private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    var searchPhrase = SearchBox.Text;
        //    MoviesList.ItemsSource = viewModel.Movies.Where(x => x.Title.Contains(searchPhrase));

        //}
    }
}
